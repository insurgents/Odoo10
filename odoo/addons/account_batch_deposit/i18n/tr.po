# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * account_batch_deposit
# 
# Translators:
# Martin Trigaux <mat@odoo.com>, 2016
# gezgin biri <gezginbiri@hotmail.com>, 2016
# Ramiz Deniz Öner <deniz@denizoner.com>, 2016
# Murat Kaplan <muratk@projetgrup.com>, 2016
# Ali Zeynel AĞCA <zeynelagca@gmail.com>, 2016
# Ayhan KIZILTAN <akiziltan76@hotmail.com>, 2016
# Azar Huseynli <azerbay1984@gmail.com>, 2016
# Ediz Duman <neps1192@gmail.com>, 2016
# Kaya Zeren <kayazeren@gmail.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-02-03 14:36+0000\n"
"PO-Revision-Date: 2017-02-03 14:36+0000\n"
"Last-Translator: Kaya Zeren <kayazeren@gmail.com>, 2017\n"
"Language-Team: Turkish (https://www.transifex.com/odoo/teams/41243/tr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: tr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. module: account_batch_deposit
#: code:addons/account_batch_deposit/models/account_payment.py:42
#, python-format
msgid ""
"All payments to print as a deposit slip must belong to the same journal."
msgstr "Kasa fişi olarak yazdırılacak tüm ödemeler aynı günlüğe ait olmalı."

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_amount
#: model:ir.ui.view,arch_db:account_batch_deposit.print_batch_deposit
msgid "Amount"
msgstr "Tutar"

#. module: account_batch_deposit
#: model:ir.model.fields,help:account_batch_deposit.field_account_journal_batch_deposit_sequence_id
msgid "Automatically generates references for batch deposits."
msgstr "Toplu para yatırma işlemleri için otomatik olarak referanslar üretir."

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_journal_id
msgid "Bank"
msgstr "Banka"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "Bank Journal"
msgstr "Banka Fişi"

#. module: account_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_account_bank_statement
msgid "Bank Statement"
msgstr "Banka Hesap Özeti"

#. module: account_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_account_bank_statement_line
msgid "Bank Statement Line"
msgstr "Banka Hesap Özeti Kalemi"

#. module: account_batch_deposit
#: model:account.payment.method,name:account_batch_deposit.account_payment_method_batch_deposit
#: model:ir.actions.act_window,name:account_batch_deposit.action_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_account_batch_deposit
#: model:ir.ui.menu,name:account_batch_deposit.menu_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.account_journal_dashboard_kanban_view_inherited
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "Batch Deposit"
msgstr "Toplu Mevduat"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_journal_batch_deposit_sequence_id
msgid "Batch Deposit Sequence"
msgstr "Toplu Ödeme Sırası"

#. module: account_batch_deposit
#: code:addons/account_batch_deposit/models/account_journal.py:43
#, python-format
msgid "Batch Deposits Sequence"
msgstr "Toplu Çek Sıralama"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_payment_batch_deposit_id
msgid "Batch deposit id"
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_journal_batch_deposit_payment_method_selected
msgid "Batch deposit payment method selected"
msgstr ""

#. module: account_batch_deposit
#: model:ir.actions.act_window,help:account_batch_deposit.action_batch_deposit
msgid ""
"Batch deposits allows you to group received checks before you deposit them to the bank.\n"
"                    The amount deposited to your bank will then appear as a single transaction on your bank statement.\n"
"                    When you proceed with the reconciliation, simply select the corresponding batch deposit to reconcile the payments."
msgstr ""
"Toplu ödemeler, alınan çekleri bankaya yatırmadan önce gruplamanıza izin verir.\n"
"Bankanıza yatırılan tutar, banka ekstrenizde tek bir işlem olarak görünür.\n"
"Uzlaştırmaya geçtiğinizde ödemeleri eşleştirmek için ilgili toplu ödemeyi seçin."

#. module: account_batch_deposit
#: code:addons/account_batch_deposit/models/account_journal.py:68
#: model:ir.actions.server,name:account_batch_deposit.action_account_create_batch_deposit
#, python-format
msgid "Create Batch Deposit"
msgstr "Toplu Kayıt Oluştur"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_create_uid
msgid "Created by"
msgstr "Oluşturan"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_create_date
msgid "Created on"
msgstr "Oluşturulma"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_currency_id
msgid "Currency id"
msgstr "Para Birimi ID"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.print_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Customer"
msgstr "Müşteri"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_date
#: model:ir.ui.view,arch_db:account_batch_deposit.print_batch_deposit
msgid "Date"
msgstr "Tarih"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_display_name
#: model:ir.model.fields,field_description:account_batch_deposit.field_report_account_batch_deposit_print_batch_deposit_display_name
msgid "Display Name"
msgstr "Adı Göstermek"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "Group By"
msgstr "Grupla"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_id
#: model:ir.model.fields,field_description:account_batch_deposit.field_report_account_batch_deposit_print_batch_deposit_id
msgid "ID"
msgstr "ID"

#. module: account_batch_deposit
#. openerp-web
#: code:addons/account_batch_deposit/static/src/js/account_reconciliation_widgets.js:94
#, python-format
msgid "Incorrect Operation"
msgstr "Geçersiz İşlem"

#. module: account_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_account_journal
msgid "Journal"
msgstr "Yevmiye"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit___last_update
#: model:ir.model.fields,field_description:account_batch_deposit.field_report_account_batch_deposit_print_batch_deposit___last_update
msgid "Last Modified on"
msgstr "Son Güncelleme"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_write_uid
msgid "Last Updated by"
msgstr "Son Güncelleyen"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_write_date
msgid "Last Updated on"
msgstr "Son Güncelleme"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.print_batch_deposit
msgid "Memo"
msgstr "Kısa Not"

#. module: account_batch_deposit
#: selection:account.batch.deposit,state:0
msgid "New"
msgstr "Yeni"

#. module: account_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_account_payment
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_payment_ids
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Payments"
msgstr "Ödemeler"

#. module: account_batch_deposit
#: code:addons/account_batch_deposit/models/account_payment.py:38
#, python-format
msgid ""
"Payments to print as a deposit slip must have 'Batch Deposit' selected as "
"payment method, not be part of an existing batch deposit and not have "
"already been reconciled"
msgstr ""
"Para yatırma fişi olarak yazdırılacak ödemeler, ödeme yöntemi olarak 'Toplu "
"Para Yatırma' seçilmelidir; mevcut bir toplu para yatırma aracının parçası "
"olmamalıdır ve daha önce uzlaştırılmamıştır."

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Print"
msgstr "Yazdır"

#. module: account_batch_deposit
#: model:ir.actions.report.xml,name:account_batch_deposit.action_print_batch_deposit
msgid "Print Batch Deposit"
msgstr "Toplu Ödemeyi Yazdır"

#. module: account_batch_deposit
#: model:ir.actions.server,name:account_batch_deposit.action_account_print_batch_deposit
msgid "Print Batch Deposits"
msgstr "Toplu Ödemeleri Yazdır"

#. module: account_batch_deposit
#: selection:account.batch.deposit,state:0
msgid "Printed"
msgstr "Yazdırıldı"

#. module: account_batch_deposit
#: selection:account.batch.deposit,state:0
msgid "Reconciled"
msgstr "Uzlaştırılmış"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_name
msgid "Reference"
msgstr "Kaynak"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Register Payment"
msgstr "Ödeme Kaydet"

#. module: account_batch_deposit
#. openerp-web
#: code:addons/account_batch_deposit/static/src/xml/account_reconciliation.xml:8
#, python-format
msgid "Select a Batch Deposit"
msgstr "Toplu Para Yatırma Seçimi"

#. module: account_batch_deposit
#. openerp-web
#: code:addons/account_batch_deposit/static/src/js/account_reconciliation_widgets.js:91
#, python-format
msgid ""
"Some journal items from the selected batch deposit are already selected in "
"another reconciliation : "
msgstr ""
"Seçilen toplu ödemedeki bazı yevmiye öğeleri zaten başka bir uzlaştırmada "
"seçildi:"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_state
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "State"
msgstr "İl/Eyalet"

#. module: account_batch_deposit
#: model:ir.model.fields,help:account_batch_deposit.field_account_journal_batch_deposit_payment_method_selected
msgid ""
"Technical feature used to know whether batch deposit was enabled as payment "
"method."
msgstr ""
"Toplu ödemenin ödeme yöntemi olarak etkinleştirilip etkinleştirilmediğini "
"bilmekte kullanılan teknik özellik."

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Total"
msgstr "Toplam"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "Unreconciled"
msgstr "Uzlaştırılmamış"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "auto ..."
msgstr "otomatik ..."

#. module: account_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_report_account_batch_deposit_print_batch_deposit
msgid "report.account_batch_deposit.print_batch_deposit"
msgstr "report.account_batch_deposit.print_batch_deposit"
