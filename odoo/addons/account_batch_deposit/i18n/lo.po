# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * account_batch_deposit
# 
# Translators:
# ສີສຸວັນ ສັງບົວບຸລົມ <sisouvan@gmail.com>, 2017
# Phoxaysy Sengchanthanouvong <phoxaysy@gmail.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-02-03 14:36+0000\n"
"PO-Revision-Date: 2017-02-03 14:36+0000\n"
"Last-Translator: Phoxaysy Sengchanthanouvong <phoxaysy@gmail.com>, 2017\n"
"Language-Team: Lao (https://www.transifex.com/odoo/teams/41243/lo/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: lo\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. module: account_batch_deposit
#: code:addons/account_batch_deposit/models/account_payment.py:42
#, python-format
msgid ""
"All payments to print as a deposit slip must belong to the same journal."
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_amount
#: model:ir.ui.view,arch_db:account_batch_deposit.print_batch_deposit
msgid "Amount"
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,help:account_batch_deposit.field_account_journal_batch_deposit_sequence_id
msgid "Automatically generates references for batch deposits."
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_journal_id
msgid "Bank"
msgstr ""

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "Bank Journal"
msgstr ""

#. module: account_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_account_bank_statement
msgid "Bank Statement"
msgstr "ບັນຊີສຳຮອງເງິນຝາກທະນາຄານ"

#. module: account_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_account_bank_statement_line
msgid "Bank Statement Line"
msgstr "ລາຍການບັນຊີສຳຮອງເງິນຝາກທະນາຄານ"

#. module: account_batch_deposit
#: model:account.payment.method,name:account_batch_deposit.account_payment_method_batch_deposit
#: model:ir.actions.act_window,name:account_batch_deposit.action_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_account_batch_deposit
#: model:ir.ui.menu,name:account_batch_deposit.menu_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.account_journal_dashboard_kanban_view_inherited
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "Batch Deposit"
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_journal_batch_deposit_sequence_id
msgid "Batch Deposit Sequence"
msgstr ""

#. module: account_batch_deposit
#: code:addons/account_batch_deposit/models/account_journal.py:43
#, python-format
msgid "Batch Deposits Sequence"
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_payment_batch_deposit_id
msgid "Batch deposit id"
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_journal_batch_deposit_payment_method_selected
msgid "Batch deposit payment method selected"
msgstr ""

#. module: account_batch_deposit
#: model:ir.actions.act_window,help:account_batch_deposit.action_batch_deposit
msgid ""
"Batch deposits allows you to group received checks before you deposit them to the bank.\n"
"                    The amount deposited to your bank will then appear as a single transaction on your bank statement.\n"
"                    When you proceed with the reconciliation, simply select the corresponding batch deposit to reconcile the payments."
msgstr ""

#. module: account_batch_deposit
#: code:addons/account_batch_deposit/models/account_journal.py:68
#: model:ir.actions.server,name:account_batch_deposit.action_account_create_batch_deposit
#, python-format
msgid "Create Batch Deposit"
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_create_uid
msgid "Created by"
msgstr "ສ້າງ ໂດຍ"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_create_date
msgid "Created on"
msgstr "ສ້າງ ເມື່ອ"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_currency_id
msgid "Currency id"
msgstr ""

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.print_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Customer"
msgstr "ລູກຄ້າ"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_date
#: model:ir.ui.view,arch_db:account_batch_deposit.print_batch_deposit
msgid "Date"
msgstr "ວັນທີ"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_display_name
#: model:ir.model.fields,field_description:account_batch_deposit.field_report_account_batch_deposit_print_batch_deposit_display_name
msgid "Display Name"
msgstr "ຊື່ທີ່ສະແດງອອກ"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "Group By"
msgstr "ຈັດກຸ່ມ ໂດຍ"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_id
#: model:ir.model.fields,field_description:account_batch_deposit.field_report_account_batch_deposit_print_batch_deposit_id
msgid "ID"
msgstr "ເລກລຳດັບ"

#. module: account_batch_deposit
#. openerp-web
#: code:addons/account_batch_deposit/static/src/js/account_reconciliation_widgets.js:94
#, python-format
msgid "Incorrect Operation"
msgstr ""

#. module: account_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_account_journal
msgid "Journal"
msgstr "ບັນຊີປະຈຳວັນ"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit___last_update
#: model:ir.model.fields,field_description:account_batch_deposit.field_report_account_batch_deposit_print_batch_deposit___last_update
msgid "Last Modified on"
msgstr "ແກ້ໄຂລ້າສຸດ ເມື່ອ"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_write_uid
msgid "Last Updated by"
msgstr "ປັບປຸງລ້າສຸດ ໂດຍ"

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_write_date
msgid "Last Updated on"
msgstr "ປັບປຸງລ້າສຸດ ເມື່ອ"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.print_batch_deposit
msgid "Memo"
msgstr ""

#. module: account_batch_deposit
#: selection:account.batch.deposit,state:0
msgid "New"
msgstr "ສ້າງໃໝ່"

#. module: account_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_account_payment
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_payment_ids
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Payments"
msgstr "ການຊໍາລະ"

#. module: account_batch_deposit
#: code:addons/account_batch_deposit/models/account_payment.py:38
#, python-format
msgid ""
"Payments to print as a deposit slip must have 'Batch Deposit' selected as "
"payment method, not be part of an existing batch deposit and not have "
"already been reconciled"
msgstr ""

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Print"
msgstr "ພິມອອກ"

#. module: account_batch_deposit
#: model:ir.actions.report.xml,name:account_batch_deposit.action_print_batch_deposit
msgid "Print Batch Deposit"
msgstr ""

#. module: account_batch_deposit
#: model:ir.actions.server,name:account_batch_deposit.action_account_print_batch_deposit
msgid "Print Batch Deposits"
msgstr ""

#. module: account_batch_deposit
#: selection:account.batch.deposit,state:0
msgid "Printed"
msgstr ""

#. module: account_batch_deposit
#: selection:account.batch.deposit,state:0
msgid "Reconciled"
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_name
msgid "Reference"
msgstr ""

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Register Payment"
msgstr ""

#. module: account_batch_deposit
#. openerp-web
#: code:addons/account_batch_deposit/static/src/xml/account_reconciliation.xml:8
#, python-format
msgid "Select a Batch Deposit"
msgstr ""

#. module: account_batch_deposit
#. openerp-web
#: code:addons/account_batch_deposit/static/src/js/account_reconciliation_widgets.js:91
#, python-format
msgid ""
"Some journal items from the selected batch deposit are already selected in "
"another reconciliation : "
msgstr ""

#. module: account_batch_deposit
#: model:ir.model.fields,field_description:account_batch_deposit.field_account_batch_deposit_state
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "State"
msgstr "ແຂວງ"

#. module: account_batch_deposit
#: model:ir.model.fields,help:account_batch_deposit.field_account_journal_batch_deposit_payment_method_selected
msgid ""
"Technical feature used to know whether batch deposit was enabled as payment "
"method."
msgstr ""

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "Total"
msgstr "ລວມທັງໝົດ"

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_search
msgid "Unreconciled"
msgstr ""

#. module: account_batch_deposit
#: model:ir.ui.view,arch_db:account_batch_deposit.view_batch_deposit_form
msgid "auto ..."
msgstr ""

#. module: account_batch_deposit
#: model:ir.model,name:account_batch_deposit.model_report_account_batch_deposit_print_batch_deposit
msgid "report.account_batch_deposit.print_batch_deposit"
msgstr ""
