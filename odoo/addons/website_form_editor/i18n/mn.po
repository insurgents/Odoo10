# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * website_form_editor
# 
# Translators:
# Martin Trigaux <mat@odoo.com>, 2017
# Khishigbat Ganbold <khishigbat@asterisk-tech.mn>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-09-07 09:58+0000\n"
"PO-Revision-Date: 2016-09-07 09:58+0000\n"
"Last-Translator: Khishigbat Ganbold <khishigbat@asterisk-tech.mn>, 2017\n"
"Language-Team: Mongolian (https://www.transifex.com/odoo/teams/41243/mn/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: mn\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: website_form_editor
#. openerp-web
#: code:addons/website_form_editor/static/src/xml/website_form_editor.xml:11
#, python-format
msgid "&times;"
msgstr "&times;"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.s_website_form
msgid ""
"<span class=\"btn btn-primary btn-lg o_website_form_send\">Send</span>\n"
"                        <span id=\"o_website_form_result\"/>"
msgstr ""
"<span class=\"btn btn-primary btn-lg o_website_form_send\">Илгээх</span>\n"
"                        <span id=\"o_website_form_result\"/>"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Add a custom field"
msgstr "Өөриймшүүлсэн талбар нэмэх"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Add a model field"
msgstr "Моделийн талбар нэмэх"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Change Form Parameters"
msgstr "Маягтын параметруудыг өөрчлөх"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Checkbox"
msgstr "Чекбокс"

#. module: website_form_editor
#. openerp-web
#: code:addons/website_form_editor/static/src/xml/website_form_editor.xml:21
#, python-format
msgid "Close"
msgstr "Хаах"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Date"
msgstr "Огноо"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Date &amp; Time"
msgstr "Огноо &amp; Цаг"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Decimal Number"
msgstr "Аравтын тоо"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "File Upload"
msgstr "Файл ачааллах"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Hidden"
msgstr "Нуугдсан"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Long Text"
msgstr "Урт текст"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Multiple Checkboxes"
msgstr "Олон чекбоксууд"

#. module: website_form_editor
#. openerp-web
#: code:addons/website_form_editor/static/src/xml/website_form_editor.xml:202
#, python-format
msgid "No matching record !"
msgstr "Таарах бичлэг алга !"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Number"
msgstr "Дугаар"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Radio Buttons"
msgstr "Радио Товчнууд"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Required"
msgstr "Шаардлагатай"

#. module: website_form_editor
#. openerp-web
#: code:addons/website_form_editor/static/src/xml/website_form_editor.xml:22
#, python-format
msgid "Save"
msgstr "Хадгалах"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Selection"
msgstr "Сонголт"

#. module: website_form_editor
#: model:ir.ui.view,arch_db:website_form_editor.snippet_options
msgid "Text"
msgstr "Текст"
