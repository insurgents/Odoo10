# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * timesheet_grid
# 
# Translators:
# Renzo Meister <info@jamotion.ch>, 2016
# Tina Rittmüller <tr@ife.de>, 2016
# Martin Trigaux <mat@odoo.com>, 2016
# Henry Mineehen <info@mineehen.de>, 2016
# aNj <anj2j@yahoo.de>, 2016
# Ralf Hilgenstock <rh@dialoge.info>, 2016
# darenkster <inactive+darenkster@transifex.com>, 2016
# Fabian Liesch <fabian.liesch@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-09-07 09:57+0000\n"
"PO-Revision-Date: 2016-09-07 09:57+0000\n"
"Last-Translator: Fabian Liesch <fabian.liesch@gmail.com>, 2016\n"
"Language-Team: German (https://www.transifex.com/odoo/teams/41243/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: timesheet_grid
#: model:ir.actions.act_window,name:timesheet_grid.action_timesheet_all
#: model:ir.ui.menu,name:timesheet_grid.menu_timesheet_grid_all
msgid "All Timesheets"
msgstr ""

#. module: timesheet_grid
#: code:addons/timesheet_grid/models/models.py:46
#, python-format
msgid "All selected timesheets are already validated"
msgstr ""

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_project_project_allow_timesheets
#: model:ir.ui.view,arch_db:timesheet_grid.project_view_form_timesheets
msgid "Allow timesheets"
msgstr ""

#. module: timesheet_grid
#: model:ir.model,name:timesheet_grid.model_account_analytic_line
msgid "Analytic Line"
msgstr "Analytischer Buchungssatz"

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.validable_form
msgid "Cancel"
msgstr "Abbrechen"

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid
msgid "Click to add projects and tasks"
msgstr ""

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validable_create_uid
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validation_create_uid
msgid "Created by"
msgstr "Erstellt von"

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validable_create_date
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validation_create_date
msgid "Created on"
msgstr "Angelegt am"

#. module: timesheet_grid
#: model:ir.model.fields,help:timesheet_grid.field_hr_employee_timesheet_validated
msgid "Date until which the employee's timesheets have been validated"
msgstr ""

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_form
msgid "Describe your activity"
msgstr ""

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validable_display_name
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validation_display_name
msgid "Display Name"
msgstr "Anzeigename"

#. module: timesheet_grid
#: model:ir.model,name:timesheet_grid.model_hr_employee
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validable_employee_id
msgid "Employee"
msgstr "Mitarbeiter"

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid_search
msgid "Group By"
msgstr "Gruppiert nach..."

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validable_id
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validation_id
msgid "ID"
msgstr "ID"

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validable___last_update
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validation___last_update
msgid "Last Modified on"
msgstr "Zuletzt geändert am"

#. module: timesheet_grid
#: model:ir.ui.menu,name:timesheet_grid.menu_timesheet_grid_validate_previous_month
msgid "Last Month"
msgstr "Letzter Monat"

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validable_write_uid
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validation_write_uid
msgid "Last Updated by"
msgstr "Zuletzt aktualisiert durch"

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validable_write_date
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validation_write_date
msgid "Last Updated on"
msgstr "Zuletzt aktualisiert am"

#. module: timesheet_grid
#: model:ir.ui.menu,name:timesheet_grid.menu_timesheet_grid_validate_previous_week
msgid "Last Week"
msgstr "Letzte Woche"

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_form
msgid "Line edition and creation from grid"
msgstr ""

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid
msgid "Month"
msgstr "Monat"

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid_validation_search
msgid "My Team"
msgstr "Mein Team"

#. module: timesheet_grid
#: model:ir.actions.act_window,name:timesheet_grid.action_timesheet_current
#: model:ir.ui.menu,name:timesheet_grid.menu_timesheet_grid_my
msgid "My Timesheet"
msgstr "Meine Zeiterfassung"

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid_search
msgid "My Timesheets"
msgstr "Meine Stundenzettel"

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid_search
msgid "Non Validated"
msgstr ""

#. module: timesheet_grid
#: code:addons/timesheet_grid/models/models.py:68
#, python-format
msgid ""
"Only a Timesheets Officer is allowed to create an entry older than the "
"validation limit."
msgstr ""

#. module: timesheet_grid
#: code:addons/timesheet_grid/models/models.py:83
#, python-format
msgid "Only a Timesheets Officer is allowed to delete a validated entry."
msgstr ""

#. module: timesheet_grid
#: code:addons/timesheet_grid/models/models.py:77
#, python-format
msgid "Only a Timesheets Officer is allowed to modify a validated entry."
msgstr ""

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid_search
msgid "Project"
msgstr "Projekt"

#. module: timesheet_grid
#: model:ir.model.fields,help:timesheet_grid.field_account_analytic_line_is_timesheet
msgid "Set if this analytic line represents a line of timesheet."
msgstr ""

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid_search
msgid "Task"
msgstr "Aufgabe"

#. module: timesheet_grid
#: code:addons/timesheet_grid/models/models.py:41
#, python-format
msgid "There aren't any timesheet to validate"
msgstr ""

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_form
msgid "Time Spent"
msgstr "Geleistete Stunden"

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid_search
msgid "Timesheet"
msgstr "Zeiterfassung"

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_account_analytic_line_is_timesheet
msgid "Timesheet Line"
msgstr "Zeiterfassungsposition"

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_list
msgid "Timesheet Lines"
msgstr ""

#. module: timesheet_grid
#: model:ir.actions.act_window,name:timesheet_grid.action_timesheet_previous_month
#: model:ir.actions.act_window,name:timesheet_grid.action_timesheet_previous_week
msgid "Timesheets To Validate"
msgstr ""

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_hr_employee_timesheet_validated
msgid "Timesheets validation limit"
msgstr ""

#. module: timesheet_grid
#: model:ir.ui.menu,name:timesheet_grid.menu_timesheet_grid_validate
msgid "To Validate"
msgstr ""

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid_search
msgid "Users"
msgstr "Benutzer"

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validation_validable_ids
msgid "Validable ids"
msgstr ""

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validable_validate
#: model:ir.ui.view,arch_db:timesheet_grid.validable_form
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid_validate
msgid "Validate"
msgstr "Bestätigen"

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.validable_form
msgid "Validate Timesheets"
msgstr ""

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.validable_form
msgid "Validate the timesheets of the selected employees up to"
msgstr ""

#. module: timesheet_grid
#: model:ir.model.fields,help:timesheet_grid.field_timesheet_grid_validable_validate
msgid "Validate this employee's timesheet up to the chosen date"
msgstr ""

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validation_validate_to
msgid "Validate to"
msgstr ""

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid_search
msgid "Validated"
msgstr "Bestätigt"

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_account_analytic_line_validated
msgid "Validated line"
msgstr ""

#. module: timesheet_grid
#: model:ir.model.fields,field_description:timesheet_grid.field_timesheet_grid_validable_validation_id
msgid "Validation id"
msgstr ""

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid
msgid "Week"
msgstr "Woche"

#. module: timesheet_grid
#: model:ir.model,name:timesheet_grid.model_timesheet_grid_validable
msgid "timesheet_grid.validable"
msgstr ""

#. module: timesheet_grid
#: model:ir.model,name:timesheet_grid.model_timesheet_grid_validation
msgid "timesheet_grid.validation"
msgstr ""

#. module: timesheet_grid
#: model:ir.ui.view,arch_db:timesheet_grid.view_timesheet_grid
msgid "you will be able to register your working hours on the given task"
msgstr ""
